<?php

namespace Tests\Feature;

use App\Fetcher;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FetcherTest extends TestCase
{
     /**
     * Fetcher test example.
     * @expectedException \Exception
     *
     * @return void
     */
    public function testExample()
    {
        Fetcher::fetch('wrong.domain', '/wrong_path_to_API');
    }
}
