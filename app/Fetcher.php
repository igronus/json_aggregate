<?php

namespace App;

class Fetcher
{
    /**
     * Fetch data
     *
     * @param $host
     * @param $endpoint
     *
     * @return string
     * @throws \Exception
     */
    public static function fetch($host, $endpoint)
    {
        $url = sprintf('%s%s', $host, $endpoint);

        $result = @file_get_contents($url);

        if ($result === false) {
            throw new \Exception(sprintf('No data from URL: %s', $url));
        }

        return $result;
    }
}
