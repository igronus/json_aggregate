<?php

namespace App\Http\Controllers;

use App\Fetcher;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JsonSchema\Validator;
use JsonSchema\Constraints\Constraint;


class HomeController extends Controller
{
    public function index(Request $request)
    {
        try {
            $json = Fetcher::fetch(env('API_HOST'), env('API_ENDPOINT'));
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return view('error', compact('error'));
        }


        $data = json_decode($json);

        if (json_last_error() !== JSON_ERROR_NONE) {
            $error = 'Fetched data is not a JSON';
            return view('error', compact('error'));
        }


        $schema = @file_get_contents(sprintf('%s/schema.json', base_path()));
        $validator = new Validator();

        $validator->validate(
            $data, json_decode($schema), Constraint::CHECK_MODE_COERCE_TYPES
        );

        if ( ! $validator->isValid()) {
            $error = 'JSON does not validate';

            if (env('APP_DEBUG')) {
                $error = 'JSON does not validate. Errors are: ';

                foreach ($validator->getErrors() as $jsonError) {
                    $error .= sprintf("[%s] %s;", $jsonError['property'], $jsonError['message']);
                }
            }

            return view('error', compact('error'));
        }


        $sum = 0; $numbers = 0; $departments = [];


        foreach ($data->data as $record) {
            if (isset($departments[$record->department_id])) {
                $departments[$record->department_id]['numbers']++;
                $departments[$record->department_id]['salary'] += $record->salary;
            } else {
                $departments[$record->department_id]['numbers'] = 1;
                $departments[$record->department_id]['salary'] = $record->salary;
            }

            $sum += $record->salary;
            $numbers++;
        }


        $avg_sum = $sum/$numbers;

        foreach ($departments as $key => $department) {
            $departments[$key]['avg_sum'] =  $department['salary']/$department['numbers'];
        }


        $days = Carbon::now()->subMonth()->daysInMonth;
        $hours = 8;

        return view('index', compact('avg_sum', 'departments', 'days', 'hours', 'json'));
    }
}
