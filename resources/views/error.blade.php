@extends('layout')

@section('content')
    <div class="m-b-md error">
        {{ $error }}
    </div>
@endsection
