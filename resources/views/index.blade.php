@extends('layout')

@section('content')
    <div class="m-b-md">
        <h1>Total</h1>

        Average salary:
        {{ number_format($avg_sum, 2) }}<br>
        Average salary per day:
        {{ number_format($avg_sum/$days, 2) }}<br>
        Average salary per hour:
        {{ number_format($avg_sum/($days*$hours), 2) }}<br>

        @foreach($departments as $key => $department)
            <h1>Department {{ $key }}</h1>

            Average salary:
            {{ number_format($department['avg_sum'], 2) }}<br>
            Average salary per day:
            {{ number_format($department['avg_sum']/$days, 2) }}<br>
            Average salary per hour:
            {{ number_format($department['avg_sum']/($days*$hours), 2) }}<br>
        @endforeach

        @if(11||env('APP_DEBUG'))
            <h1>Source JSON</h1>
            <pre>{{ $json }}</pre>
        @endif
    </div>
@endsection
