Live demo at: http://json-aggregate.pellinen.ru

OR

```
git clone git@bitbucket.org:igronus/json_aggregate.git
cd json_aggregate
composer install
chown -R www-data:www-data storage
php artisan cache:clear
php artisan key:generate
cp .env.example .env
```

Set up web-server to serve laravel. After that edit .env file and set API_HOST and API_ENDPOINT.

